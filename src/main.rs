use std::{env, path, sync::Arc};

use axum::{extract::State, response::Html, routing::get, Router};
use minijinja::Environment;

#[tokio::main]
async fn main() {
    let home_var = env::var("HOME").expect("HOME variable not set");
    let data_path = path::Path::new(&env::var("XDG_DATA_HOME").unwrap_or_else(|_| {
        path::Path::new(&home_var)
            .join(".local")
            .join("share")
            .to_str()
            .expect("Invalid HOME value")
            .to_string()
    }))
    .join("recipes");

    println!("{:?}", data_path);

    let config_path = path::Path::new(&env::var("XDG_CONFIG_HOME").unwrap_or_else(|_| {
        path::Path::new(&home_var)
            .join(".config")
            .to_str()
            .expect("Invalid HOME value")
            .to_string()
    }))
    .join("recipes.toml");

    println!("{:?}", config_path);

    let mut template_environment = Environment::new();
    template_environment.set_loader(minijinja::path_loader("templates"));

    let arc_template = Arc::new(template_environment);

    let app = Router::new().route("/", get(home)).with_state(arc_template);

    axum::Server::bind(&"0.0.0.0:3000".parse().unwrap())
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn home(State(template): State<Arc<Environment<'static>>>) -> Html<&'static str> {
    let template = template.get_template("index.html").unwrap();

    Html(
        r#"
             <!DOCTYPE html>
<html>
<body>

<h1>My First Heading</h1>
<p>My first paragraph.</p>

</body>
</html> 
        "#,
    )
}
